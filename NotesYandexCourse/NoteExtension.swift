import Foundation
import UIKit

extension Note {
    
    var json: [String: Any] {
        get {
            var json = [String: Any]()

            json["uid"] = self.uid
            json["title"] = self.title
            json["content"] = self.content
            if (self.color != .white) {
                json["color"] = self.color.toHexString()
            }
            if (self.importance != .average) {
                json["importance"] = self.importance.rawValue
            }
            let timeInterval = selfDestructionDate?.timeIntervalSince1970
            if (timeInterval != nil) {
                json["selfDestructionDate"] = Int(timeInterval!)
            }
            
            return json
        }
    }
    
    static func parse(json: [String: Any]) -> Note? {
        let uid = json["uid"] as? String
        let title = json["title"] as? String
        let content = json["content"] as? String
        guard uid != nil && title != nil && content != nil else {
            return nil
        }
        
        let colorValue = json["color"] as? String
        var color: UIColor = UIColor.white
        
        let importanceValue = json["importance"] as? Int
        var importance = NoteImportance.average
        
        if (colorValue != nil) {
            let resolvedColor = UIColor.init(hexValue: colorValue!)
            color = resolvedColor
        }
        
        if (importanceValue != nil) {
            let resolvedImportance = NoteImportance(rawValue: importanceValue ?? 0)!
            importance = resolvedImportance
        }
        
        let selfDestructionDateValue = json["selfDestructionDate"] as? Int
        var selfDestructionDate: Date? = nil
        
        if (selfDestructionDateValue != nil) {
            let resolvedSelfDestructionDateValue = Date(timeIntervalSince1970: TimeInterval(selfDestructionDateValue!))
            selfDestructionDate = resolvedSelfDestructionDateValue
        }
        
        return Note(
            uid: uid!,
            title: title!,
            content: content!,
            color: color,
            importance: importance,
            selfDestructionDate: selfDestructionDate
        )
    }
}
