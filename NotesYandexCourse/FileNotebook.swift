import Foundation

class FileNotebook {
    
    private let fileManager = FileManager.default
    private let fileName = "storage"
    private let fileExtension = "json"
    
    public private(set) var notes: [String: Note] = [:]
    
    public func add(_ note: Note) {
        notes[note.uid] = note
    }
    
    public func remove(with uid: String) {
        notes[uid] = nil
    }
    
    public func saveToFile() {
        do {
            let filePath = getFilePath()
            let json = notes.mapValues { (Note) -> [String: Any] in
                Note.json
            }
            let jsonDict = json as [String: Any]
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            let jsonString = String(data: jsonData, encoding: .utf8)!
            try jsonString.write(to: filePath, atomically: false, encoding: .utf8)
        } catch {
            // пока игнорируем
        }
    }
    
    public func loadFromFile() {
        do {
            let filePath = getFilePath()
            let jsonString = try String(contentsOf: filePath, encoding: .utf8)
            let jsonData = jsonString.data(using: .utf8)!
            let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options : .allowFragments) as? [String: Any]
            
            notes = [:]
            jsonObject?.forEach({ (arg0) in
                let (key, value) = arg0
                notes[key] = Note.parse(json: value as! [String: Any])
            })
        }
        catch {
            // пока игнорируем
        }
    }
    
    private func getBaseDirPath() -> URL {
        return fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!
    }
    
    private func getFilePath() -> URL {
        let dirPath = getBaseDirPath()
        return dirPath.appendingPathComponent(fileName).appendingPathExtension(fileExtension)
    }
}
