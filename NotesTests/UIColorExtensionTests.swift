import XCTest
import UIKit

class UIColorExtensionTests: XCTestCase {
    
    func testColorToHexifiedStringValue() {
        let color = UIColor.white
        let hexColor = color.toHexString()
        let expectedValue = "#ffffff"
        XCTAssert(hexColor == expectedValue, "Invalid hexified color value. Expected \(expectedValue)")
    }
    
    func testHexifiedStringValueToColor() {
        let hexColor = "#ffff00"
        let resolvedColor = UIColor(hexValue: hexColor)
        let expectedValue = UIColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 1.0)
        XCTAssert(resolvedColor == expectedValue, "Invalid resolved from hexified color value")
    }
}
