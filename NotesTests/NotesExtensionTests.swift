import XCTest

class NotesExtensionTests: XCTestCase {
    
    func testSuccessfullySerializedDeserialized() {
        let originalNote = Note(title: "Title of note", content: "Lorem ipsum dolor sit amet")
        let serializedNote = originalNote.json
        let desrializedNote = Note.parse(json: serializedNote)
        
        XCTAssertNotNil(desrializedNote, "Note deserialization returned nil")
        
        let contentOfNotesAreEqual = originalNote.uid == desrializedNote!.uid
            && originalNote.title == desrializedNote!.title
            && originalNote.content == desrializedNote!.content
            && originalNote.color == desrializedNote!.color
            && originalNote.importance == desrializedNote!.importance
            && originalNote.selfDestructionDate == desrializedNote!.selfDestructionDate
        
        XCTAssertTrue(contentOfNotesAreEqual, "Contents of original note and serializid/deserialized note are not equal")
    }
}
